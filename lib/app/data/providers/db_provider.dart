import 'dart:io';
import 'dart:developer';

import 'package:csv/csv.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:money_logs/app/core/models/account_sub_head_model.dart';
import 'package:money_logs/app/core/models/type_fields.dart';
import 'package:money_logs/app/core/models/type_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../../core/models/account_sub_head_field.dart';
import '../../core/models/my_bar_chart_data.dart';
import '../enums/enums.dart';
import '../services/settings_service.dart';
import '../../core/models/account_head_fields.dart';
import '../../core/models/account_head_model.dart';
import '../../core/models/income_expanse_model.dart';
import '../../core/models/income_expense_fields.dart';
import '../../modules/register/user_model.dart';

class MoneyLogsDatabase {
  static final MoneyLogsDatabase instance = MoneyLogsDatabase._init();
  static Database? _database;
  final SettingsService _settingsService = Get.put(SettingsService());

  final String userTableName = 'user';

  MoneyLogsDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB('money_logs.db');
    return _database!;
  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(
      path,
      version: 1,
      onCreate: _createDB,
      onConfigure: _onConfigure,
    );
  }

  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    const intType = 'INTEGER NOT NULL';

    log('Creating User Table');
    db.execute('''
      CREATE TABLE  $userTableName(
        id $idType,
        full_name $textType,
        username $textType,
        password $textType 
      )
    ''');

    log("Creating Type Table");
    db.execute('''
      CREATE TABLE ${TypeFields.typeTableName} (
        ${TypeFields.id} $idType,
        ${TypeFields.name} $textType,
        ${TypeFields.createdAt} $textType,
        ${TypeFields.createdBy} $textType,
        FOREIGN KEY (${TypeFields.createdBy}) REFERENCES user (id)               
        ON DELETE NO ACTION 
      )
    ''');

    log('Creating Account Head Table');
    db.execute('''
      CREATE TABLE ${AccountHeadFields.accountHeadTableName} (
        ${AccountHeadFields.id} $idType,
        ${AccountHeadFields.title} $textType UNIQUE,
        ${AccountHeadFields.headAmount} TEXT NULL,
        ${AccountHeadFields.typeId} $intType,
        ${IncomeExpenseFields.createdBy} $intType,
        ${IncomeExpenseFields.createdAt} $textType,
        FOREIGN KEY (${AccountHeadFields.createdBy}) REFERENCES user (id)               
        ON DELETE NO ACTION,
        FOREIGN KEY (${AccountHeadFields.typeId})
        REFERENCES ${TypeFields.typeTableName} (id)               
        ON DELETE NO ACTION
      )
    ''');

    log('Creating Account Sub Head Table');
    db.execute('''
      CREATE TABLE ${AccountSubHeadFields.accountSubHeadTableName} (
        ${AccountSubHeadFields.id} $idType,
        ${AccountSubHeadFields.title} $textType UNIQUE,
        ${AccountSubHeadFields.headId} $intType,
        ${AccountSubHeadFields.createdBy} $intType,
        ${AccountSubHeadFields.createdAt} $textType,
        FOREIGN KEY (${AccountSubHeadFields.createdBy}) REFERENCES user (id)               
        ON DELETE NO ACTION,
        FOREIGN KEY (${AccountSubHeadFields.headId}) REFERENCES 
        ${AccountHeadFields.accountHeadTableName} (id)               
        ON DELETE NO ACTION
      )
    ''');

    log('Creating Income Expanse Table');
    db.execute('''
      CREATE TABLE ${IncomeExpenseFields.incomeAndExpenseTableName} (
        ${IncomeExpenseFields.id} $idType,
        ${IncomeExpenseFields.amount} $textType,
        ${IncomeExpenseFields.details} $textType,
        ${IncomeExpenseFields.transactionDate} $textType,
        ${IncomeExpenseFields.transactionTime} $textType,
        ${IncomeExpenseFields.subHeadId} $textType,
        ${IncomeExpenseFields.createdBy} $intType,
        ${IncomeExpenseFields.createdAt} $textType,
        FOREIGN KEY (${IncomeExpenseFields.createdBy}) REFERENCES user (id)               
        ON DELETE NO ACTION,
        FOREIGN KEY (${IncomeExpenseFields.subHeadId}) REFERENCES
        ${AccountSubHeadFields.accountSubHeadTableName} (id)               
        ON DELETE NO ACTION
      )
    ''');
  }

  // Register user and add income and expense type
  Future<User> register(User user) async {
    final db = await instance.database;
    int insertId = await db.insert(userTableName, user.toJson());
    await db.insert(
      TypeFields.typeTableName,
      TypeModel(
        id: 1,
        name: "Income",
        createdAt: DateTime.now().toString(),
        createdBy: insertId.toString(),
      ).toJson(),
    );
    await db.insert(
      TypeFields.typeTableName,
      TypeModel(
        id: 2,
        name: "Expense",
        createdAt: DateTime.now().toString(),
        createdBy: insertId.toString(),
      ).toJson(),
    );
    return user.copy(id: insertId);
  }

  // Every time login user
  Future<User> login(String username, String password) async {
    final db = await instance.database;
    final userMap = await db.query(
      userTableName,
      where: 'username = ? AND password = ? ',
      whereArgs: [username, password],
    );
    // log(userMap.toString());
    if (userMap.isNotEmpty) {
      return User.fromJson(userMap.first);
    } else {
      throw Exception('User not Found');
    }
  }

  // Every time Dashboard loads app checks for Head Present or not
  Future<bool> isIncomeExpenseAccountHeadsPresent() async {
    final db = await instance.database;
    final rowCount = Sqflite.firstIntValue(
      await db.rawQuery(
        "SELECT COUNT(*) FROM ${AccountHeadFields.accountHeadTableName}",
      ),
    );
    return rowCount! > 0 ? false : true;
  }

  // TODO: Calculate Total Income
  Future<double> totalIncome() async {
    double totalIncome = 0;
    final db = await instance.database;
    String sql = ''' 
      SELECT amount from ${IncomeExpenseFields.incomeAndExpenseTableName}
    ''';
    final allMap = await db.rawQuery(sql);
    log(allMap.toString());
    return totalIncome * _settingsService.globalRate.value;
  }

  // TODO: Calculate Total Expense
  Future<double> totalExpense() async {
    double totalExpanse = 0;

    final db = await instance.database;
    String sql = ''' 
      SELECT amount from ${IncomeExpenseFields.incomeAndExpenseTableName}
    ''';
    final allMap = await db.rawQuery(sql);
    log(allMap.toString());
    return totalExpanse * _settingsService.globalRate.value;
  }

  // Add Account Head
  Future<void> addAccountHead(AccountHead accountHead) async {
    final db = await instance.database;
    await db.insert(
      AccountHeadFields.accountHeadTableName,
      accountHead.toJson(),
    );
  }

  // Add Account Sub Head
  Future<void> addSubAccountHead(AccountSubHead accountSubHead) async {
    final db = await instance.database;
    await db.insert(
      AccountSubHeadFields.accountSubHeadTableName,
      accountSubHead.toJson(),
    );
  }

  // get all sub head id by head id
  Future<List<AccountSubHead>> getSubHeadsByHeadId(int headId) async {
    final db = await instance.database;
    final subHeads = await db.query(
      AccountSubHeadFields.accountSubHeadTableName,
      where: "${AccountSubHeadFields.headId} = ?",
      whereArgs: [headId],
      orderBy: AccountHeadFields.title,
    );

    return List.generate(
      subHeads.length,
      (index) => AccountSubHead(
        id: subHeads[index][AccountSubHeadFields.id] as int,
        title: subHeads[index][AccountSubHeadFields.title] as String,
        accountHeadId: subHeads[index][AccountSubHeadFields.headId] as int,
        createdBy: subHeads[index][AccountSubHeadFields.createdBy] as int,
        createAt: subHeads[index][AccountSubHeadFields.createdAt] as String,
      ),
    );
  }

  // get all the heads by type id
  Future<List<AccountHead>> getHeadsByType(String type) async {
    final db = await instance.database;
    final incomeAccountHeadsMap = await db.query(
      AccountHeadFields.accountHeadTableName,
      where: "${AccountHeadFields.typeId} = ?",
      whereArgs: [type],
      orderBy: AccountHeadFields.title,
    );
    return List.generate(
      incomeAccountHeadsMap.length,
      (index) => AccountHead(
        id: incomeAccountHeadsMap[index][AccountHeadFields.id] as int,
        title: incomeAccountHeadsMap[index][AccountHeadFields.title] as String,
        headAmount: incomeAccountHeadsMap[index][AccountHeadFields.headAmount]
            as String,
        typeId: incomeAccountHeadsMap[index][AccountHeadFields.typeId] as int,
        createAt:
            incomeAccountHeadsMap[index][AccountHeadFields.createdAt] as String,
        createdBy:
            incomeAccountHeadsMap[index][AccountHeadFields.createdBy] as int,
      ),
    );
  }

  // get all the sub heads by head id
  Future<List<AccountSubHead>> getSubHeadsByHead(int headId) async {
    final db = await instance.database;
    final accountSubHeadsMap = await db.query(
      AccountSubHeadFields.accountSubHeadTableName,
      where: "${AccountSubHeadFields.headId} = ?",
      whereArgs: [headId.toString()],
      orderBy: AccountSubHeadFields.title,
    );
    log(accountSubHeadsMap.length.toString());
    return List.generate(
      accountSubHeadsMap.length,
      (index) => AccountSubHead(
        id: accountSubHeadsMap[index][AccountSubHeadFields.id] as int,
        title: accountSubHeadsMap[index][AccountSubHeadFields.title] as String,
        accountHeadId:
            accountSubHeadsMap[index][AccountSubHeadFields.headId] as int,
        createAt:
            accountSubHeadsMap[index][AccountHeadFields.createdAt] as String,
        createdBy:
            accountSubHeadsMap[index][AccountHeadFields.createdBy] as int,
      ),
    );
  }

  Future<List<AccountHead>> incomeExpenseAccountHeads() async {
    final db = await instance.database;
    final incomeAccountHeadsMap = await db.query(
      AccountHeadFields.accountHeadTableName,
      orderBy: "title",
    );
    log(incomeAccountHeadsMap.toString());
    return List.generate(
      incomeAccountHeadsMap.length,
      (index) => AccountHead(
        id: incomeAccountHeadsMap[index][AccountHeadFields.id] as int,
        title: incomeAccountHeadsMap[index][AccountHeadFields.title] as String,
        headAmount: incomeAccountHeadsMap[index][AccountHeadFields.headAmount]
            as String,
        typeId: incomeAccountHeadsMap[index][AccountHeadFields.typeId] as int,
        createAt:
            incomeAccountHeadsMap[index][AccountHeadFields.createdAt] as String,
        createdBy:
            incomeAccountHeadsMap[index][AccountHeadFields.createdBy] as int,
      ),
    );
  }

  Future<List<MyBarChartData>> incomeExpenseAccountHeadsForBarChart() async {
    final db = await instance.database;
    final incomeAccountHeadsMap = await db.query(
      AccountHeadFields.accountHeadTableName,
      orderBy: "name DESC",
    );
    var year = DateTime.now().year;
    var month = DateTime.now().month;
    var day = DateTime.now().day;
    //
    List<MyBarChartData> data = [];
    for (var i = 0; i < incomeAccountHeadsMap.length; i++) {
      final incomeAccHeadAmountMap = await db.rawQuery(
        '''SELECT SUM(${IncomeExpenseFields.amount}) as amount, head_name, type
         FROM  ${IncomeExpenseFields.incomeAndExpenseTableName} WHERE
         ${IncomeExpenseFields.details} =
         '${incomeAccountHeadsMap[i][AccountHeadFields.title]}' AND
         ${IncomeExpenseFields.transactionDate} BETWEEN
         '$year-0$month-01' AND '$year-0$month-$day'
         GROUP BY ${IncomeExpenseFields.details}' ''',
      );
      //  log("Total Sum: $incomeAccHeadAmountMap");
      if (incomeAccHeadAmountMap.isNotEmpty) {
        data.add(
          MyBarChartData(
            id: i,
            headName: incomeAccHeadAmountMap[0]['head_name'].toString(),
            y: double.parse(incomeAccHeadAmountMap[0]['amount'].toString()),
            type: int.parse(incomeAccHeadAmountMap[0]['type'].toString()),
          ),
        );
      } else {
        // log("I : $i AND: ${incomeAccountHeadsMap[i]['name']}");
        data.add(
          MyBarChartData(
            id: i,
            headName: incomeAccountHeadsMap[i]['name'].toString(),
            y: 0,
            type: int.parse(incomeAccountHeadsMap[i]['type'].toString()),
          ),
        );
      }
    }
    //log("Data: $data");
    return data;
  }

  // Future<bool> showDeleteButton(AccountHead accountHead) async {
  //   final db = await instance.database;
  //   final rowCount = Sqflite.firstIntValue(
  //     await db.rawQuery(
  //         '''
  //         SELECT COUNT(*) FROM ${IncomeExpenseFields.incomeAndExpenseTableName}
  //         WHERE ${IncomeExpenseFields} = '${accountHead}'
  //         '''
  //     ),
  //   );
  //   // log(rowCount.toString());
  //   return rowCount! > 0 ? false : true;
  // }

  Future<void> deleteAccountHead(AccountHead accountHead) async {
    final db = await instance.database;
    await db.delete(
      AccountHeadFields.accountHeadTableName,
      where: "id = ?",
      whereArgs: [accountHead.id],
    );
  }

  Future<void> addIncome(IncomeExpanse income) async {
    final db = await instance.database;
    await db.insert(
        IncomeExpenseFields.incomeAndExpenseTableName, income.toJson());
  }

  Future<void> addExpense(IncomeExpanse expense) async {
    final db = await instance.database;
    await db.insert(
        IncomeExpenseFields.incomeAndExpenseTableName, expense.toJson());
  }

  Future<List<IncomeExpanse>> allIncomeExpanseStatements() async {
    final db = await instance.database;
    final incomeMap = await db.query(
      IncomeExpenseFields.incomeAndExpenseTableName,
      orderBy: 'transaction_date DESC, transaction_time DESC',
    );
    // log(incomeMap.toString());
    return List<IncomeExpanse>.generate(
      incomeMap.length,
      (index) => IncomeExpanse(
        id: incomeMap[index][IncomeExpenseFields.id] as int,
        amount: incomeMap[index][IncomeExpenseFields.amount] as String,
        details: incomeMap[index][IncomeExpenseFields.details] as String,
        subHeadId: incomeMap[index][IncomeExpenseFields.subHeadId] as String,
        createdBy: incomeMap[index][IncomeExpenseFields.createdBy] as int,
        createdAt: incomeMap[index][IncomeExpenseFields.createdAt] as String,
        transactionDate:
            incomeMap[index][IncomeExpenseFields.transactionDate] as String,
        transactionTime:
            incomeMap[index][IncomeExpenseFields.transactionTime] as String,
      ),
    );
  }

  Future dbToCsvExport() async {
    final db = await instance.database;
    final incomeMap =
        await db.query(IncomeExpenseFields.incomeAndExpenseTableName);
    final List<List<dynamic>> listOfIncomeAndExpense = [
      [
        "id",
        "amount",
        "details",
        "transaction_date",
        "transaction_time",
        "type",
        "head_name",
        "created_by",
        "created_at"
      ]
    ];
    // log("Map: " + incomeMap.toString());
    for (var data in incomeMap) {
      // log("Data: " + data.toString());
      listOfIncomeAndExpense.add([
        data["id"],
        data["amount"],
        data["details"],
        data["transaction_date"],
        data["transaction_time"],
        data["type"],
        data["head_name"],
        data["created_by"],
        data["created_at"]
      ]);
    }
    log(listOfIncomeAndExpense.toString());
    var csvIncomeExpense =
        const ListToCsvConverter().convert(listOfIncomeAndExpense);
    log("CSV: $csvIncomeExpense");
    final String directory = "/storage/emulated/0/Download";
    final path =
        "$directory/money-logs-csv-${DateFormat("dd-MM-yyyy").format(DateTime.now())}.csv";
    log("Path: $path");
    final File file = File(path);
    await file.writeAsString(csvIncomeExpense);
  }

  Future<void> deleteIncomeExpenseStatement(id) async {
    final db = await instance.database;
    db.delete(IncomeExpenseFields.incomeAndExpenseTableName,
        where: "id = ?", whereArgs: [id]);
  }

  Future<double> filteredTotalIncome(String startDate, String endDate) async {
    double totalIncome = 0;
    final db = await instance.database;
    final allMap = await db.rawQuery(
        "SELECT ${IncomeExpenseFields.amount} FROM ${IncomeExpenseFields.incomeAndExpenseTableName} WHERE  ${IncomeExpenseFields.transactionDate} BETWEEN '$startDate' AND '$endDate' AND type = 1;");
    // " '2022-01-31' ;");
    for (var element in allMap) {
      totalIncome += double.parse(element["amount"].toString());
    }
    return totalIncome * _settingsService.globalRate.value;
  }

  Future<double> filteredTotalExpense(String startDate, String endDate) async {
    double totalExpanse = 0;
    final db = await instance.database;
    final allMap = await db.rawQuery(''' 
        SELECT ${IncomeExpenseFields.amount} FROM 
        ${IncomeExpenseFields.incomeAndExpenseTableName} WHERE  
        ${IncomeExpenseFields.transactionDate} BETWEEN '$startDate' AND 
        '$endDate' AND type = 2;
        ''');
    for (var element in allMap) {
      totalExpanse += double.parse(element["amount"].toString());
    }
    return totalExpanse * _settingsService.globalRate.value;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
