import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsService extends GetxService {
  late SharedPreferences _preferences;
  final isOnBoardingViewed = 0.obs;
  final isUserAdded = 0.obs;
  final userList = [].obs;
  final RxDouble globalRate = 1.0.obs;

  Future<SettingsService> init() async {
    debugPrint('$runtimeType initialize shared preference');
    _preferences = await SharedPreferences.getInstance();
    debugPrint('$runtimeType shared preference is ready');
    isOnBoardingViewed.value = (_preferences.getInt('isOnBoardingViewed') ?? 0);
    isUserAdded.value = (_preferences.getInt('isUserAdded') ?? 0);
    globalRate.value = (_preferences.getDouble("rate") ?? 1.0);
    return this;
  }

  viewed() {
    _preferences.setInt('isOnBoardingViewed', 1);
    isOnBoardingViewed.value = _preferences.getInt('isOnBoardingViewed')!;
  }

  userAdded(user) {
    _preferences.setInt("isUserAdded", 1);
    _preferences.setStringList("user", user);
    userList.value = user;
    isUserAdded.value = _preferences.getInt("isUserAdded")!;
  }

  changeRate(double rate) {
    _preferences.setDouble("rate", rate);
    globalRate.value = rate;
  }
}
