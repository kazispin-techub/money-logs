import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/data/services/settings_service.dart';

import '../../../core/models/exchangerate_model.dart';
import '../../../core/models/providers/exchangerate_provider.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/strings.dart';
import '../controllers/currency_controller.dart';

class CurrencyView extends GetView<CurrencyController> {
  final ExchangeRateProvider _exchangeRateProvider = ExchangeRateProvider();
  final CurrencyController _currencyController = Get.find<CurrencyController>();
  final SettingsService _settingsService = Get.put(SettingsService());

  final rateCountryList = [
    "BDT",
    "AED",
    "AUD",
    "CAD",
    "CNY",
    "EUR",
    "GBP",
    "INR",
    "JPY",
    "LKR",
    "NZD",
    "OMR",
    "PKR",
    "SAR",
    "USD",
    "YER",
  ];

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appbar: AppBar(title: Text('Change Currency'), centerTitle: true),
      body: Column(
        children: [
          FutureBuilder(
            future: _exchangeRateProvider.getExchangeRate(),
            builder: (context, AsyncSnapshot<ExchangeRate> snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return errorSection(snapshot);
                } else if (snapshot.data == null) {
                  return noDataFoundSection();
                } else if (snapshot.hasData) {
                  // log(snapshot.data!.conversionRates!.toString());
                  return dataShowSection(snapshot);
                }
              }
              return const Center(child: CircularProgressIndicator());
            },
          )
        ],
      ),
    );
  }

  errorSection(snapshot) {
    return Center(
      child:
          Text(snapshot.error.toString(), style: const TextStyle(fontSize: 18)),
    );
  }

  Widget noDataFoundSection() {
    return Expanded(
      child: Center(child: Text('No Data!', style: TextStyles.normal16Black)),
    );
  }

  dataShowSection(AsyncSnapshot<ExchangeRate> snapshot) {
    return Expanded(
      child: ListView(
        children: rateCountryList
            .map(
              (country) => Obx(
                () => RadioListTile<String?>(
                  title: Text(country),
                  value: country,
                  groupValue: _currencyController.currentRate.value,
                  onChanged: (value) {
                    log(value!);
                    _currencyController.currentRate(value);
                    changeRate(value, snapshot.data!.conversionRates!);
                  },
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  changeRate(String rate, ConversionRates value) {
    switch (rate) {
      case "AED":
        _settingsService.changeRate(value.aED!);
        break;
      case "AUD":
        _settingsService.changeRate(value.aUD!);
        break;
      case "CAD":
        _settingsService.changeRate(value.cAD!);
        break;
      case "CNY":
        _settingsService.changeRate(value.cNY!);
        break;
      case "EUR":
        _settingsService.changeRate(value.eUR!);
        break;
      case "GBP":
        _settingsService.changeRate(value.gBP!);
        break;
      case "INR":
        _settingsService.changeRate(value.iNR!);
        break;
      case "JPY":
        _settingsService.changeRate(value.jPY!);
        break;
      case "LKR":
        _settingsService.changeRate(value.lKR!);
        break;
      case "NZD":
        _settingsService.changeRate(value.nZD!);
        break;
      case "OMR":
        _settingsService.changeRate(value.oMR!);
        break;
      case "PKR":
        _settingsService.changeRate(value.pKR!);
        break;
      case "SAR":
        _settingsService.changeRate(value.sAR!);
        break;
      case "USD":
        _settingsService.changeRate(value.uSD!);
        break;
      case "YER":
        _settingsService.changeRate(value.yER!);
        break;
      default:
        _settingsService.changeRate(1.0);
        break;
    }
    log(_settingsService.globalRate.value.toString());
    Get.offNamed("/dashboard");
  }
}
