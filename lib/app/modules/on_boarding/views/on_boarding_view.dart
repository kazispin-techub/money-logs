import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/theme/button_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/strings.dart';
import '../controllers/on_boarding_controller.dart';

class OnBoardingView extends GetView<OnBoardingController> {
  final OnBoardingController _onBoardingController =
      Get.find<OnBoardingController>();
  @override
  Widget build(BuildContext context) {
    return BlankLayout(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          mainLogoAndTagLineSection(),
          middleTagLineSection(),
          _bottomSectionWithButton(context, _onBoardingController),
        ],
      ),
    );
  }

  Column mainLogoAndTagLineSection() {
    return Column(
      children: [
        SizedBox(height: 100),
        Image.asset("assets/images/money_logs_logo.png", height: 200),
        Text("Welcome To", style: TextStyles.semiBold48WhiteE5),
        Text("Money Logs", style: TextStyles.semiBold48WhiteE5),
      ],
    );
  }

  Column middleTagLineSection() {
    return Column(
      children: [
        Text(
          "It's time to seek financial",
          style: TextStyles.normal16Black,
        ),
        Text("advice from experts", style: TextStyles.normal16Black),
      ],
    );
  }

  Container _bottomSectionWithButton(
      BuildContext context, OnBoardingController onBoardingController) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      child: OnBoardingButton(
        onTap: () {
          _onBoardingController.settingsService.viewed();
          Get.offNamed("/register");
        },
      ),
    );
  }
}
