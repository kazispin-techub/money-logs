import 'package:get/get.dart';

import '../../../data/services/settings_service.dart';
import '../controllers/sign_in_controller.dart';

class SignInBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SignInController>(
      () => SignInController(
        settingsService: Get.find<SettingsService>(),
      ),
    );
  }
}
