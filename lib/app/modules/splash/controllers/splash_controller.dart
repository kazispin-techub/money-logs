import 'package:get/get.dart';

import '../../../data/services/settings_service.dart';

class SplashController extends GetxController {
  final SettingsService settingsService;

  SplashController({required this.settingsService});

  void navigateUser() {
    if (settingsService.isOnBoardingViewed.value == 1) {
      if (settingsService.isUserAdded.value == 1) {
        Get.offNamed('/sign-in');
      } else {
        Get.offNamed('/register');
      }
    } else {
      Get.offNamed('/on-boarding');
    }
  }
}
