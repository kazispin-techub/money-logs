import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/theme/layout_theme.dart';
import '../../../core/values/strings.dart';
import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  final SplashController _splashController = Get.find<SplashController>();
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 3), _splashController.navigateUser);
    return BlankLayout(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("assets/images/money_logs_logo.png", height: 200),
          Text("Money Logs", style: TextStyles.semiBold48WhiteE5)
        ],
      ),
    );
  }
}
