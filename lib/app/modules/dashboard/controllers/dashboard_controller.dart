import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/models/income_expanse_model.dart';
import '../../../core/values/colors.dart';
import '../../../data/providers/db_provider.dart';

class DashboardController extends GetxController {
  late RxDouble totalIncomeDouble = 0.0.obs;
  late RxDouble totalExpenseDouble = 0.0.obs;
  late bool isHeads;

  Future<List<IncomeExpanse>> getAllIncomeExpanse() async {
    return await MoneyLogsDatabase.instance.allIncomeExpanseStatements();
  }

  deleteNow(id) async {
    await MoneyLogsDatabase.instance.deleteIncomeExpenseStatement(id);
    totalIncomeDouble.value = await MoneyLogsDatabase.instance.totalIncome();
    totalExpenseDouble.value = await MoneyLogsDatabase.instance.totalExpense();
  }

  Future<void> deleteStatement(id) async {
    Get.defaultDialog(
      title: "Are You Sure",
      content: Text(""),
      buttonColor: buttonRed,
      confirmTextColor: white,
      onConfirm: () {
        deleteNow(id);
        update();
        Get.back();
      },
      onCancel: () {
        Get.back();
      },
    );
  }

  @override
  void onInit() async {
    totalIncomeDouble.value = await MoneyLogsDatabase.instance.totalIncome();
    totalExpenseDouble.value = await MoneyLogsDatabase.instance.totalExpense();
    isHeads =
        await MoneyLogsDatabase.instance.isIncomeExpenseAccountHeadsPresent();
    if (isHeads) {
      Get.defaultDialog(
          title: "No Account Head Found",
          middleText: "Please add one before.",
          textConfirm: "Add Heads",
          confirmTextColor: whiteE5,
          buttonColor: buttonGreen,
          textCancel: "Later",
          cancelTextColor: buttonRed,
          onConfirm: () {
            Get.offNamed("/account-head");
          },
          onCancel: () {
            Get.back();
          });
    }
    super.onInit();
  }
}
