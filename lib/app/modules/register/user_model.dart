class User {
  int? id;
  String? fullName;
  String? username;
  String? password;

  User({this.id, this.fullName, this.username, this.password});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    // data['id'] = id;
    data['full_name'] = fullName;
    data['username'] = username;
    data['password'] = password;
    return data;
  }

  User copy({
    int? id,
    String? fullName,
    String? username,
    String? password,
  }) =>
      User(
          id: id ?? this.id,
          fullName: fullName ?? this.fullName,
          username: username ?? this.username,
          password: password ?? this.password);
}
