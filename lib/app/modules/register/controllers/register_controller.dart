import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/providers/db_provider.dart';
import '../../../data/services/settings_service.dart';
import '../user_model.dart';

class RegisterController extends GetxController {
  final SettingsService settingsService;

  RegisterController({required this.settingsService});
  final GlobalKey<FormState> registerFormKey = GlobalKey<FormState>();

  final TextEditingController fullNameCtl = TextEditingController();
  final TextEditingController usernameCtl = TextEditingController();
  final TextEditingController passwordCtl = TextEditingController();
  final TextEditingController confirmPasswordCtl = TextEditingController();

  RxBool isLoading = false.obs;

  String? fullNameValidation(String fullName) {
    if (fullName.isEmpty) {
      return "Name Can't be empty";
    }
    return null;
  }

  String? usernameValidation(String username) {
    if (username.isEmpty) {
      return "Username Can't be empty";
    } else if (!GetUtils.isUsername(username)) {
      return "It is not a valid username";
    }
    return null;
  }

  String? passwordValidation(String password) {
    if (password.isEmpty) {
      return "Password Can't be empty";
    }
    return null;
  }

  String? confirmPasswordValidation(String password) {
    if (password.isEmpty) {
      return "Password Can't be empty";
    } else if (passwordCtl.text != password) {
      return "Password not matching";
    }
    return null;
  }

  void register() async {
    final bool isValid = registerFormKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    registerFormKey.currentState!.save();

    isLoading.value = true;

    await MoneyLogsDatabase.instance.register(
      User(
        fullName: fullNameCtl.text,
        username: usernameCtl.text,
        password: passwordCtl.text,
      ),
    );
    settingsService.userAdded([fullNameCtl.text, usernameCtl.text]);
    isLoading.value = false;

    Get.offNamed("/dashboard");
  }
}
