import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/core/values/colors.dart';

import '../../../core/theme/button_theme.dart';
import '../../../core/theme/input_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/strings.dart';
import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  final RegisterController _registerController = Get.find<RegisterController>();

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appbar: AppBar(
        toolbarHeight: 0,
        shadowColor: transparent,
        backgroundColor: transparent,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: mainSection(),
        ),
      ),
    );
  }

  Column mainSection() {
    return Column(
      children: [
        SizedBox(height: 100),
        Image.asset("assets/images/money_logs_logo.png", height: 100),
        SizedBox(height: 20),
        Text("Welcome Onboard", style: TextStyles.medium25Black),
        SizedBox(height: 40),
        Form(
          key: _registerController.registerFormKey,
          child: Column(
            children: [
              fullNameForm(),
              usernameForm(),
              passwordForm(),
              confirmPasswordForm(),
            ],
          ),
        ),
        SizedBox(height: 20),
        Obx(
          () => _registerController.isLoading.value
              ? Center(child: CircularProgressIndicator())
              : SubmitButton(
                  onTap: () {
                    _registerController.register();
                  },
                  buttonColor: buttonGreen,
                  title: "Register",
                ),
        ),
      ],
    );
  }

  CustomInputField fullNameForm() {
    return CustomInputField(
      hint: 'Your Full Name',
      controller: _registerController.fullNameCtl,
      validator: (fullName) {
        return _registerController.fullNameValidation(fullName);
      },
      onSaved: (fullName) {
        _registerController.fullNameCtl.text = fullName;
      },
    );
  }

  CustomInputField usernameForm() {
    return CustomInputField(
      hint: 'Your User Name',
      controller: _registerController.usernameCtl,
      validator: (username) {
        return _registerController.usernameValidation(username);
      },
      onSaved: (username) {
        _registerController.usernameCtl.text = username;
      },
    );
  }

  CustomInputField passwordForm() {
    return CustomInputField(
      hint: 'Your Secret',
      controller: _registerController.passwordCtl,
      validator: (password) {
        return _registerController.passwordValidation(password);
      },
      onSaved: (password) {
        _registerController.passwordCtl.text = password;
      },
      isHidden: true,
    );
  }

  CustomInputField confirmPasswordForm() {
    return CustomInputField(
      hint: 'Confirm Your Secret',
      controller: _registerController.confirmPasswordCtl,
      validator: (password) {
        return _registerController.confirmPasswordValidation(password);
      },
      onSaved: (password) {},
      isHidden: true,
    );
  }
}
