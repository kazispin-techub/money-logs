import 'package:get/get.dart';
import 'package:flutter/material.dart';

import '../../../core/models/account_sub_head_model.dart';
import '../controllers/account_head_controller.dart';

class AccountSubHeadSection extends GetView<AccountHeadController> {
  final List<AccountSubHead> accountSubHead;

  AccountSubHeadSection({required this.accountSubHead});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: ListView.builder(
        itemCount: accountSubHead.length,
        itemBuilder: (BuildContext ctx, index) {
          return ListTile(
            title: Text("${index + 1}. ${accountSubHead[index].title}"),
          );
        },
      ),
    );
  }
}
