import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../../../data/enums/enums.dart';
import '../../../data/providers/db_provider.dart';
import '../controllers/account_head_controller.dart';
import '../widgets/account_head_section.dart';

class AccountHeadView extends GetView<AccountHeadController> {
  final AccountHeadController _accountHeadController =
      Get.find<AccountHeadController>();

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      isDrawer: true,
      appbar: AppBar(title: Text('Account Heads')),
      body: Container(
        decoration: BoxDecoration(),
        child: GetBuilder<AccountHeadController>(builder: (logic) {
          return FutureBuilder(
            future: MoneyLogsDatabase.instance.incomeExpenseAccountHeads(),
            builder: (
              BuildContext context,
              AsyncSnapshot<List<AccountHead>> snapshot,
            ) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return errorSection();
                } else if (snapshot.data!.isEmpty) {
                  return noDataSection();
                } else if (snapshot.hasData) {
                  return AccountHeadSection(accHead: snapshot);
                }
              }
              return const Center(child: CircularProgressIndicator());
            },
          );
        }),
      ),
      isFloatingButton: true,
      floatingActionButton: floatingButton(),
    );
  }

  Widget floatingButton() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FloatingActionButton(
          heroTag: "Income",
          backgroundColor: buttonGreen,
          child: const Icon(Icons.add),
          onPressed: () {
            addCategoryDialog(AccountHeadType.income.index);
          },
        ),
        SizedBox(height: 10),
        FloatingActionButton(
          heroTag: "Expense",
          backgroundColor: buttonRed,
          child: const Icon(Icons.add),
          onPressed: () {
            addCategoryDialog(AccountHeadType.expense.index);
          },
        ),
      ],
    );
  }

  addCategoryDialog(int accountHeadType) {
    String myTitle = "";
    if (accountHeadType == 1) {
      myTitle = "New Income Head";
    } else {
      myTitle = "New Expense Head";
    }
    return Get.defaultDialog(
      title: myTitle,
      textConfirm: "Add",
      confirmTextColor: whiteE5,
      buttonColor: accountHeadType == 1 ? buttonGreen : buttonRed,
      textCancel: "Cancel",
      cancelTextColor: buttonRed,
      content: Column(children: [accountHeadName(), accountHeadAmount()]),
      onConfirm: () async {
        await addNewHead(accountHeadType);
      },
    );
  }

  Future<void> addNewHead(int accountHeadType) async {
    if (_accountHeadController.accountHeadCtl.text.isEmpty) {
      Get.snackbar("Error", "Head can't be empty");
    } else {
      try {
        await MoneyLogsDatabase.instance.addAccountHead(
          AccountHead(
            title: _accountHeadController.accountHeadCtl.text,
            headAmount: _accountHeadController.accountHeadAmountCtl.text,
            typeId: accountHeadType,
            createdBy: 1,
            createAt: DateFormat('dd-MM-yyyy').format(DateTime.now()),
          ),
        );
        Get.back();
      } catch (e) {
        log(e.toString());
        Get.back();
        Get.snackbar(
          "Error",
          "Head name must be unique",
          colorText: buttonRed,
        );
      } finally {
        _accountHeadController.accountHeadCtl.text = "";
        _accountHeadController.accountHeadAmountCtl.text = "";
        _accountHeadController.update();
      }
    }
  }

  Widget accountHeadName() {
    return TextFormField(
      controller: _accountHeadController.accountHeadCtl,
      decoration: InputDecoration(hintText: "Head Title"),
    );
  }

  Widget accountHeadAmount() {
    return TextFormField(
      controller: _accountHeadController.accountHeadAmountCtl,
      decoration: InputDecoration(hintText: "Head Budget Amount"),
      keyboardType: TextInputType.number,
    );
  }

  Widget noDataSection() {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("Add an"),
          SizedBox(height: 10),
          Text('"Account HEAD"', style: TextStyles.bold24),
          SizedBox(height: 10),
          Text(" and Add Budget Amount to Each "),
          SizedBox(height: 10),
          Text('"HEAD"', style: TextStyles.bold24),
          SizedBox(height: 10),
          Text("Example: "),
          SizedBox(height: 10),
          Text("Food - 10000TK", style: TextStyles.bold24),
          SizedBox(height: 10),
          Text("Then add"),
          SizedBox(height: 10),
          Text('"SUB HEAD"', style: TextStyles.bold24),
          SizedBox(height: 10),
          Text("to each head."),
        ],
      ),
    );
  }

  Center errorSection() {
    return Center(
      child: Text('Error occurred', style: const TextStyle(fontSize: 18)),
    );
  }
}
