import 'dart:developer';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/core/models/my_bar_chart_data.dart';

import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../../../data/providers/db_provider.dart';
import '../controllers/chart_controller.dart';

class ChartView extends GetView<ChartController> {
  final ChartController _chartController = Get.find<ChartController>();

  final monthsList = [
    "Life Time",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      isDrawer: true,
      appbar: AppBar(
        title: Text('Chart'),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future:
            MoneyLogsDatabase.instance.incomeExpenseAccountHeadsForBarChart(),
        builder: (
          BuildContext context,
          AsyncSnapshot<List<MyBarChartData>> snapshot,
        ) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return errorSection(snapshot);
            } else if (snapshot.data!.isEmpty) {
              return noDataFoundSection(context);
            } else if (snapshot.hasData) {
              log(snapshot.data!.toString());
              return dataShowSection(context, snapshot);
            }
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget noDataFoundSection(context) {
    return SizedBox(
      child: Center(child: const Text("Please add income, expense first")),
      height: 100,
    );
  }

  Center errorSection(snapshot) {
    return Center(
      // child: Text('${snapshot.error}', style: const TextStyle(fontSize: 18)),
      child: Text(
        'Something Went Wrong, Please Restart the APP',
        style: const TextStyle(fontSize: 18),
      ),
    );
  }

  Widget dataShowSection(context, AsyncSnapshot<List<dynamic>> snapshot) {
    return Obx(
      () {
        dynamic maxNumber = snapshot.data?.first;
        for (var element in snapshot.data!) {
          if (element.y >= maxNumber.y) {
            maxNumber = element;
          }
        }
        log(maxNumber.y.toString());
        return SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(38.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.5,
                  decoration: BoxDecoration(border: Border.all(color: black)),
                  child: PieChart(
                    PieChartData(
                      sections: [
                        PieChartSectionData(
                          title: "Income",
                          titleStyle: TextStyles.normal16White,
                          color: buttonGreen,
                          value: _chartController.incomeValue.value,
                          radius: 150,
                        ),
                        PieChartSectionData(
                          title: "Expense",
                          titleStyle: TextStyles.normal16White,
                          color: buttonRed,
                          value: _chartController.expenseValue.value,
                          radius: 150,
                        ),
                      ],
                      centerSpaceRadius: 0,
                      sectionsSpace: 2,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              DropdownButton<String>(
                value: _chartController.selectedMonth.value,
                items: monthsList
                    .map(
                      (e) => DropdownMenuItem<String>(
                        value: e,
                        child: Text(
                          e,
                          style: TextStyles.normal16Black,
                        ),
                      ),
                    )
                    .toList(),
                onChanged: (value) async {
                  _chartController.selectedMonth(value);
                  await _chartController.getIncomeValueByFilter(value!);
                },
              ),
              SizedBox(height: 50),
              Text(
                "This Month Report",
                style: TextStyles.medium25Black,
              ),
              SizedBox(height: 50),
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.7,
                child: BarChart(
                  BarChartData(
                      maxY: maxNumber.y > 10000 ? maxNumber.y : 10000,
                      minY: 0,
                      barTouchData: BarTouchData(enabled: true),
                      titlesData: FlTitlesData(
                        leftTitles: AxisTitles(
                          sideTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 50,
                          ),
                        ),
                        rightTitles: AxisTitles(
                          sideTitles: SideTitles(showTitles: false),
                        ),
                        topTitles: AxisTitles(
                          sideTitles: SideTitles(showTitles: false),
                        ),
                        bottomTitles: AxisTitles(
                          sideTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 200,
                            getTitlesWidget: (id, title) => RotatedBox(
                              quarterTurns: 1,
                              child: Text(
                                snapshot.data!
                                    .firstWhere(
                                        (element) => element.id == id.toInt())
                                    .headName,
                              ),
                            ),
                          ),
                        ),
                      ),
                      barGroups: snapshot.data!
                          .map(
                            (e) => BarChartGroupData(
                              x: e.id,
                              barRods: [
                                BarChartRodData(
                                  toY: e.y,
                                  width: 10,
                                  color: e.type == 1 ? buttonGreen : buttonRed,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(6),
                                    topRight: Radius.circular(6),
                                  ),
                                ),
                              ],
                            ),
                          )
                          .toList()),
                  swapAnimationDuration:
                      Duration(milliseconds: 150), // Optional
                  swapAnimationCurve: Curves.bounceIn, // Optional
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
