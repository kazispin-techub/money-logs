// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const ON_BOARDING = _Paths.ON_BOARDING;
  static const REGISTER = _Paths.REGISTER;
  static const SIGN_IN = _Paths.SIGN_IN;
  static const CATEGORY = _Paths.CATEGORY;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const INCOME = _Paths.INCOME;
  static const EXPENSE = _Paths.EXPENSE;
  static const ACCOUNT_HEAD = _Paths.ACCOUNT_HEAD;
  static const SETTINGS = _Paths.SETTINGS;
  static const CHART = _Paths.CHART;
  static const IMPORT_EXPORT = _Paths.IMPORT_EXPORT;
  static const CURRENCY = _Paths.CURRENCY;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const ON_BOARDING = '/on-boarding';
  static const REGISTER = '/register';
  static const SIGN_IN = '/sign-in';
  static const CATEGORY = '/category';
  static const DASHBOARD = '/dashboard';
  static const INCOME = '/income';
  static const EXPENSE = '/expense';
  static const ACCOUNT_HEAD = '/account-head';
  static const SETTINGS = '/settings';
  static const CHART = '/chart';
  static const IMPORT_EXPORT = '/import-export';
  static const CURRENCY = '/currency';
}
