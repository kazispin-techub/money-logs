class AccountSubHeadFields {
  static final String accountSubHeadTableName = 'account_sub_head';
  static final List<String> values = [id, title, headId, createdBy, createdAt];
  static const String id = 'id';
  static const String title = 'title';
  static const String headId = 'ac_head_id';
  static const String createdBy = 'created_by';
  static const String createdAt = 'created_at';
}
