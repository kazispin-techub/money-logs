import 'account_head_fields.dart';

class AccountHead {
  int? id;
  late String title;
  late String headAmount;
  late int typeId;
  late int createdBy;
  late String createAt;

  AccountHead({
    this.id,
    required this.title,
    required this.headAmount,
    required this.typeId,
    required this.createdBy,
    required this.createAt,
  });

  AccountHead.fromJson(Map<String, dynamic> json) {
    id = json[AccountHeadFields.id];
    title = json[AccountHeadFields.title];
    headAmount = json[AccountHeadFields.headAmount];
    typeId = json[AccountHeadFields.typeId];
    createdBy = json[AccountHeadFields.createdBy];
    createAt = json[AccountHeadFields.createdAt];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    // data[AccountHeadFields.id] = id;
    data[AccountHeadFields.title] = title;
    data[AccountHeadFields.headAmount] = headAmount;
    data[AccountHeadFields.typeId] = typeId;
    data[AccountHeadFields.createdBy] = createdBy;
    data[AccountHeadFields.createdAt] = createAt;
    return data;
  }
}
