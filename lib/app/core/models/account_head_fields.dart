class AccountHeadFields {
  static final String accountHeadTableName = 'account_head';
  static final List<String> values = [id, title, typeId, createdBy, createdAt];
  static const String id = 'id';
  static const String title = 'title';
  static const String headAmount = 'head_amount';
  static const String typeId = 'type_id';
  static const String createdBy = 'created_by';
  static const String createdAt = 'created_at';
}
