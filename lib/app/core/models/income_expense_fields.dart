class IncomeExpenseFields {
  static final String incomeAndExpenseTableName = 'income_and_expense';

  static final List<String> values = [
    id,
    amount,
    details,
    transactionDate,
    transactionTime,
    subHeadId,
    createdBy,
    createdAt
  ];
  static const String id = 'id';
  static const String amount = 'amount';
  static const String details = 'details';
  static const String transactionDate = 'transaction_date';
  static const String transactionTime = 'transaction_time';
  static const String subHeadId = 'sub_head_id';
  static const String createdBy = 'created_by';
  static const String createdAt = 'created_at';
}
