import 'account_sub_head_field.dart';

class AccountSubHead {
  int? id;
  late String title;
  late int accountHeadId;
  late int createdBy;
  late String createAt;

  AccountSubHead({
    this.id,
    required this.title,
    required this.accountHeadId,
    required this.createdBy,
    required this.createAt,
  });

  AccountSubHead.fromJson(Map<String, dynamic> json) {
    id = json[AccountSubHeadFields.id];
    title = json[AccountSubHeadFields.title];
    accountHeadId = json[AccountSubHeadFields.headId];
    createdBy = json[AccountSubHeadFields.createdBy];
    createAt = json[AccountSubHeadFields.createdAt];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    // data[AccountHeadFields.id] = id;
    data[AccountSubHeadFields.title] = title;
    data[AccountSubHeadFields.headId] = accountHeadId;
    data[AccountSubHeadFields.createdBy] = createdBy;
    data[AccountSubHeadFields.createdAt] = createAt;
    return data;
  }
}
