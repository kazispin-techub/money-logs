import 'package:get/get.dart';

import '../exchangerate_model.dart';

class ExchangeRateProvider extends GetConnect {
  Future<ExchangeRate> getExchangeRate() async {
    final response = await get('https://open.er-api.com/v6/latest/BDT');
    //log(response.status.code.toString());
    //log(response.body.toString());
    return ExchangeRate.fromJson(response.body);
  }
}
