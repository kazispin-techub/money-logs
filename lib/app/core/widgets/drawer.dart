import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/data/services/settings_service.dart';
import 'package:money_logs/app/modules/dashboard/controllers/dashboard_controller.dart';

import '../values/colors.dart';
import '../values/strings.dart';

class TechHubDrawer extends GetView<SettingsService> {
  final SettingsService _settingsService = Get.put(SettingsService());
  final DashboardController _dashboardController =
      Get.put(DashboardController());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
              decoration: BoxDecoration(
                color: primaryColor,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        _settingsService.userList[0],
                        style: TextStyles.normal16White,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        ((_dashboardController.totalIncomeDouble.value -
                                    _dashboardController
                                        .totalExpenseDouble.value) *
                                _settingsService.globalRate.value)
                            .toString(),
                        style: TextStyles.normal16White,
                      ),
                    ],
                  ),
                ],
              )),
          ListTile(
            leading: Icon(Icons.dashboard_outlined),
            title: const Text("Dashboard"),
            onTap: () {
              Get.offNamed('/dashboard');
            },
          ),
          ListTile(
            leading: Icon(Icons.monetization_on_outlined),
            title: const Text("Account Heads"),
            onTap: () {
              Get.offNamed('/account-head');
            },
          ),
          ListTile(
            leading: Icon(Icons.pie_chart_outline_outlined),
            title: const Text("View Chart"),
            onTap: () {
              Get.offNamed('/chart');
            },
          ),
          ListTile(
            leading: Icon(Icons.import_export_outlined),
            title: const Text("Import Export"),
            onTap: () {
              Get.offNamed('/import-export');
            },
          ),
          ListTile(
            leading: Icon(Icons.settings_outlined),
            title: const Text("Settings"),
            onTap: () {
              Get.offNamed('/settings');
            },
          ),
        ],
      ),
    );
  }
}
